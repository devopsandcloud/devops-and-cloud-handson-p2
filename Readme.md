# July 2023 Challenge
This is the excercise challenge project for July 2023 of the "DevOps and Cloud" youtube channel.
Youtube Channel: https://www.youtube.com/@DevOps-Cloud

## At the time of writing this document, there are 5 stages for 2 parts.

Optional: 
You can use this script if you want to setup your Jenkins on a container, it will include all the tools we used here.
https://github.com/sheenilim08/myPublicScripts/tree/master/general/docker

### Part 1 - Stage 1
Invokes running the script Part1/initializeProvisioner.sh, it checks your AWS if there is a EC2 instance with a tag name specified 
by your environment variable "AMI_NAME", if it already exist it will skip the creation.

### Part 2 - Stage 2
Scan code with SonarCloud, nothing much to explain here other than SonarCloud scanning your code.

### Part 2 - Stage 3
This will build the artifact

### Part 2 - Stage 4
This will dockerize the artifact and will be pushed to my custom container registry.

### Part 2 - Stage 5
This will create an EC2 instance based on the AMI created on "Part 1 - Stage 1", then will copy the docker-compose.yaml whic
contains the definition of our container from my custom container registry as well as a mySQL instance for database.
Once ready, the app will be accessible to the public IP assigned to the EC2 instance via port 8080.

NOTE: The key/pair, security group, public ip are provisioned in the script "deploy-ec2-instance.sh" which is called on this stage.