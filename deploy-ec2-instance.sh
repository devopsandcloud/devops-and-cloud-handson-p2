#!/bin/bash

ami_name="${AMI_NAME}"
region="ap-southeast-2"
instance_type="t2.micro"
key_name="ami-version-1.0.1-ssh"

ami_id=$(aws ec2 describe-images --filters "Name=name,Values=$ami_name" --query "Images[0].ImageId" --output text --region $region)

if [[ $ami_id == "None" ]]; then
  echo "AMI with name $ami_name does not exist in region $region"
  exit 1
fi

# This will download the pem key file to be used to SSH to the EC2 instance
time_stamp=$(date +"%Y%m%s")
aws ec2 create-key-pair --key-name ${key_name}-${time_stamp} --query "KeyMaterial" --output text --region $region > "${key_name}-${time_stamp}.pem"
chmod 400 "${key_name}-${time_stamp}.pem"
echo "Created key pair $key_name-$time_stamp in region $region and saved private key to $key_name-$time_stamp.pem"

# Create the security groups
if aws ec2 describe-security-groups --group-names $ami_name --region $region >/dev/null 2>&1; then
  echo "Security group $ami_name already exists in region $region"
else
  # Create a new security group and allow outbound traffic and inbound traffic on ports 8080 and 22
  aws ec2 create-security-group --group-name $ami_name --description "Security group for EC2 instance" --region $region
  aws ec2 authorize-security-group-ingress --group-name $ami_name --protocol tcp --port 8080 --cidr 0.0.0.0/0 --region $region
  aws ec2 authorize-security-group-ingress --group-name $ami_name --protocol tcp --port 22 --cidr 0.0.0.0/0 --region $region
  echo "Created security group $ami_name in region $region and allowed outbound traffic and inbound traffic on ports 8080 and 22"
fi

# Create the EC2 instance using the pem keyfile
instance_id=$(aws ec2 run-instances --image-id $ami_id --instance-type $instance_type --key-name ${key_name}-${time_stamp} --security-groups $ami_name --query "Instances[0].InstanceId" --output text --region $region)
aws ec2 create-tags --resources $instance_id --tags Key=Name,Value="$ami_name" --region $region
echo "EC2 instance launched with AMI $ami_name in region $region"

# Wait for the EC2 instance to be ready to accept connections
echo "Creating the EC2 instance."
while ! aws ec2 wait instance-status-ok --instance-ids $instance_id --region $region >/dev/null 2>&1; do 
  sleep 5; 
  echo "Waiting for EC2 instance to be ready"
done

public_ip=$(aws ec2 describe-instances --instance-ids $instance_id --query 'Reservations[0].Instances[0].PublicIpAddress' --output text --region $region)

# The docker compose ont he server is using an older version for Amazon Linux 2
cmds="bash ./deploy-app-to-ec2-instance.sh ${NEXUS_REPOOBJ_USR} ${NEXUS_REPOOBJ_PSW}"

echo "Copying deploy-app-to-ec2-instance.sh docker-compose.yaml file to EC2 Instance."
scp -o StrictHostKeyChecking=no -i "${key_name}-${time_stamp}".pem docker-compose.yaml ec2-user@${public_ip}:/home/ec2-user
scp -o StrictHostKeyChecking=no -i "${key_name}-${time_stamp}".pem deploy-app-to-ec2-instance.sh ec2-user@${public_ip}:/home/ec2-user

echo "Running docker compose on EC2 instance."
ssh -o StrictHostKeyChecking=no -i "${key_name}-${time_stamp}".pem ec2-user@${public_ip} ${cmds} 